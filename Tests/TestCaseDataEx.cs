﻿using System;
using NUnit.Framework;

namespace RedFrogs.Tests
{
    public class TestCaseDataEx : TestCaseData
    {
        public TestCaseDataEx(string name, params object[] args) : base(args)
        {
            SetName(name);
        }

        public object Expected
        {
            set
            {
                Returns(value);
            }
        }

        public Type ThrowsType
        {
            set
            {
                Throws(value);
            }
        }
    }
}