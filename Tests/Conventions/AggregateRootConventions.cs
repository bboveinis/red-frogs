﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Extensions;

namespace RedFrogs.Tests.Conventions
{
    public class AggregateRootConventions
    {
        private IEnumerable<Type> AllAggregateRootTypes
        {
            get
            {
                return Helpers.AllTypes
                    .Where(t => t.BaseType == typeof (AggregateRoot));
            }
        }
        public IEnumerable<TestCaseDataEx> AllAggregateRootCases
        {
            get
            {
                return AllAggregateRootTypes.Select(t => new TestCaseDataEx(t.FullName, t));
            }
        }

        public IEnumerable<TestCaseDataEx> AllApplyMethodCases
        {
            get
            {
                return from t in AllAggregateRootTypes
                    from m in t.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    where m.Name == "Apply"
                    select new TestCaseDataEx(
                        "{0}.Apply({1})".FormatWith(t.FullName, m.GetParameters().Select(p => p.ParameterType.Name).ToCsv()),
                        m
                    );
            }
        }


        [TestCaseSource("AllAggregateRootCases")]
        public void AllAggregateRootsHaveTheSerializableAttributeSoThatItCanBeBinaryCopied(Type aggregateRootType)
        {
            aggregateRootType.Should().BeDecoratedWith<SerializableAttribute>();
        }

        [TestCaseSource("AllAggregateRootCases")]
        public void AllAggregateRootsHaveAParameterlessContructorSoThatTheyCanBeInstantiated(Type aggregateRootType)
        {
            aggregateRootType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null).Should().NotBeNull("No parameterless constructor");
        }

        [TestCaseSource("AllApplyMethodCases")]
        public void AllAggregateRootApplyMethodsArePublic(MethodInfo applyMethod)
        {
            applyMethod.IsPublic.Should().BeTrue();
        }

        [TestCaseSource("AllApplyMethodCases")]
        public void AllAggregateRootApplyMethodsHaveOneParameter(MethodInfo applyMethod)
        {
            applyMethod.GetParameters().Should().HaveCount(1);
        }

        [TestCaseSource("AllApplyMethodCases")]
        public void AllAggregateRootApplyMethodsTakeAParameterWhoseTypeIsDerivedFromAMatchingFactAboutClosedGeneric(MethodInfo applyMethod)
        {
            var parameter = applyMethod.GetParameters().FirstOrDefault();
            if(parameter == null)
                Assert.Inconclusive("No parameters");

            var factAboutType = parameter.ParameterType.BaseType;
            while (factAboutType != null && !factAboutType.IsGenericType && factAboutType.GetGenericTypeDefinition() != typeof (FactAbout<>))
                factAboutType = factAboutType.BaseType;

            if(factAboutType == null)
                Assert.Fail("First parameter does not inherit FactAbout<T>");

            factAboutType.GetGenericArguments()[0].Should().Be(applyMethod.DeclaringType, "The apply method should take as it's first and only parameter a type that inherits from FactAbout<T> where T is the same as the type declaring the aforementioned apply method");
        }
    }
}