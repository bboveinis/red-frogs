﻿namespace RedFrogs.Shared.Domain.DomainEvents
{
    public interface IHandleDomainFact
    {
        
    }
    public interface IHandleDomainFact<in T> : IHandleDomainFact
    {
        void Handle(T fact);
    }
}