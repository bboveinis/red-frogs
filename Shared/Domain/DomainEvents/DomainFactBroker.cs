﻿using System.Collections.Generic;
using Autofac;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Shared.Domain.DomainEvents
{
    public interface IDomainFactBroker
    {
        void Raise<T>(T fact) where T : IFact;
    }

    [InstancePerLifetimeScope]
    public class DomainFactBroker : IDomainFactBroker
    {
        private readonly ILifetimeScope _scope;

        public DomainFactBroker(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public void Raise<T>(T fact) where T : IFact
        {
            var handlers = _scope.Resolve<IEnumerable<IHandleDomainFact<T>>>();
            foreach (var handler in handlers) 
                handler.Handle(fact);
        }
    }
}