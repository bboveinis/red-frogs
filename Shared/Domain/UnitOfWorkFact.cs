﻿using System;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Shared.Domain
{
    public class UnitOfWorkFact
    {
        public UnitOfWorkFact(Guid unitOfWorkId, DateTimeOffset timestamp, int ordinal, IFact fact)
        {
            UnitOfWorkId = unitOfWorkId;
            Timestamp = timestamp;
            Ordinal = ordinal;
            Fact = fact;
        }

        public Guid UnitOfWorkId { get; private set; }
        public DateTimeOffset Timestamp { get; private set; }
        public int Ordinal { get; private set; }
        public IFact Fact { get; private set;  }

    }
}