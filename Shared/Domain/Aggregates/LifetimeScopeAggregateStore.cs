﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RedFrogs.Shared.Domain.Aggregates
{
    public interface ILifetimeScopeAggregateStore<out T> where T : AggregateRoot
    {
        T Get(Guid id);
        IReadOnlyList<T> All();
    }

    public class LifetimeScopeAggregateStore<T> : ILifetimeScopeAggregateStore<T> where T : AggregateRoot
    {
        private readonly ILongTermAggregateStore<T> _longTermAggregateStore;

        public Dictionary<Guid, T> _alreadyReturned = new Dictionary<Guid, T>();

        public LifetimeScopeAggregateStore(ILongTermAggregateStore<T> longTermAggregateStore)
        {
            _longTermAggregateStore = longTermAggregateStore;
        }

        public T Get(Guid id)
        {
            T item;
            if (!_alreadyReturned.TryGetValue(id, out item))
            {
                item = _longTermAggregateStore.Get(id);
                _alreadyReturned.Add(item.Id, item);
            }
            return item;
        }

        public IReadOnlyList<T> All()
        {
            var items = _longTermAggregateStore.All().ToArray();
            for (var x = 0; x < items.Length; x++)
            {
                T alreadyReturned;
                if (_alreadyReturned.TryGetValue(items[x].Id, out alreadyReturned))
                    items[x] = alreadyReturned;
            }
            return items;
        }
    }
}