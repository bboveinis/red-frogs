﻿using System;
using System.Collections.Generic;
using System.Linq;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Domain.Facts.Stores;

namespace RedFrogs.Shared.Domain.Aggregates
{
    public interface IAggregateRebuilder<T> where T : AggregateRoot
    {
        IReadOnlyList<T> RebuildAll();
        T Rebuild(Guid id);
    }

    public class AggregateRebuilder<T> : IAggregateRebuilder<T> where T : AggregateRoot
    {
        private readonly IFactStore<T> _factStore;

        public AggregateRebuilder(IFactStore<T> factStore)
        {
            _factStore = factStore;
        }

        public IReadOnlyList<T> RebuildAll() 
        {
            return _factStore.GetFactStream()
                .Select(stream => Rebuild(stream.Value))
                .ToArray();
        }

        public T Rebuild(Guid aggregateId)
        {
            return Rebuild(_factStore.GetFactStream(aggregateId));
        }


        private static T Rebuild(IReadOnlyList<FactAbout<T>> stream)
        {
            var aggregateRoot = (AggregateRoot)Activator.CreateInstance(typeof(T), true);
            foreach (var fact in stream)
                ((dynamic)aggregateRoot).Apply((dynamic)fact);
            return (T) aggregateRoot;
        }

    }
}