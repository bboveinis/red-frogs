using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Shared.Domain.Aggregates
{
    [Serializable]
    public class AggregateRoot : IId
    {
        private ConcurrentBag<IFact> _facts = new ConcurrentBag<IFact>();

        protected AggregateRoot()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; protected set; }

        public void Append(IFact fact)
        {
            _facts.Add(fact);
        }

        public IEnumerable<IFact> GetAndClearFacts()
        {
            return Interlocked.Exchange(ref _facts, new ConcurrentBag<IFact>());
        }
    }
}