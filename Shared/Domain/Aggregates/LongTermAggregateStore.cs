﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using RedFrogs.Shared.Extensions;

namespace RedFrogs.Shared.Domain.Aggregates
{
    public interface ILongTermAggregateStore
    {
        void Invalidate(Guid[] ids);
    }

    public interface ILongTermAggregateStore<out T> : ILongTermAggregateStore where T : AggregateRoot
    {
        T Get(Guid id);
        IReadOnlyList<T> All();
    }

    public class LongTermAggregateStore<T> : ILongTermAggregateStore<T> where T : AggregateRoot
    {
        private readonly IAggregateRebuilder<T> _aggregateRebuilder;
        private readonly Lazy<ConcurrentDictionary<Guid, T>> _items;

        public LongTermAggregateStore(IAggregateRebuilder<T> aggregateRebuilder)
        {
            _aggregateRebuilder = aggregateRebuilder;
            _items = new Lazy<ConcurrentDictionary<Guid, T>>(LoadAll);
        }

        private ConcurrentDictionary<Guid, T> LoadAll()
        {
            var dict = _aggregateRebuilder.RebuildAll().ToDictionary(a => a.Id);
            return new ConcurrentDictionary<Guid, T>(dict);
        }

        public T Get(Guid id)
        {
            if (!_items.IsValueCreated)
                return _aggregateRebuilder.Rebuild(id);

            T result;
            return _items.Value.TryGetValue(id, out result)
                ? result.BinaryClone()
                : null;
        }

        public IReadOnlyList<T> All()
        {
            return _items.Value.Values.Select(a => a.BinaryClone()).ToArray();
        }

        public void Invalidate(Guid[] ids)
        {
            if (!_items.IsValueCreated)
                return;

            foreach (var id in ids)
                _items.Value.AddOrUpdate(id, i => _aggregateRebuilder.Rebuild(i), (i, e) => _aggregateRebuilder.Rebuild(i));
        }
    }
}