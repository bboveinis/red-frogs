﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Nimbus;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Domain.DomainEvents;
using RedFrogs.Shared.Domain.Facts;
using RedFrogs.Shared.Domain.Facts.Stores;
using RedFrogs.Shared.Extensions;
using RedFrogs.Shared.Infrastructure;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Shared.Domain
{
    public interface IUnitOfWork
    {
        void EnlistInTransaction(AggregateRoot item);
        void EnlistInTransaction(IReadOnlyList<AggregateRoot> items);
        Task Commit();
    }

    [InstancePerLifetimeScope]
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IBus _bus;
        private readonly ILifetimeScope _lifetimeScope;
        private readonly IDomainFactBroker _domainFactBroker;
        private readonly IUnitOfWorkBus _unitOfWorkBus;
        private readonly HashSet<AggregateRoot> _aggregateRootsInTransaction = new HashSet<AggregateRoot>();
        private readonly Guid _unitOfWorkId = Guid.NewGuid();

        public UnitOfWork(IBus bus, ILifetimeScope lifetimeScope, IDomainFactBroker domainFactBroker, IUnitOfWorkBus unitOfWorkBus)
        {
            _bus = bus;
            _lifetimeScope = lifetimeScope;
            _domainFactBroker = domainFactBroker;
            _unitOfWorkBus = unitOfWorkBus;
        }

        public void EnlistInTransaction(AggregateRoot item)
        {
            _aggregateRootsInTransaction.Add(item);
        }

        public void EnlistInTransaction(IReadOnlyList<AggregateRoot> items)
        {
            foreach (var item in items)
                _aggregateRootsInTransaction.Add(item);
        }

        public async Task Commit()
        {
            var allFacts = FactFindAndRaiseAllTheFacts().ToArray();
            if (allFacts.Any())
            {
                var now = DateTimeOffset.Now;
                var uowFacts = allFacts.Select((f, i) => new UnitOfWorkFact(_unitOfWorkId, now, i, f)).ToArray();

                await SendFactsToTheStore(uowFacts);
                await _bus.Publish(new NewFactsEvent(uowFacts)); // This is the first event we want to go out
            }
            await _unitOfWorkBus.Commit();
        }

        private async Task SendFactsToTheStore(UnitOfWorkFact[] uowFacts)
        {
            var storeTasks = uowFacts.Select(f => f.Fact.ForType).Distinct().Select(type =>
            {
                var storeType = typeof (IFactStore<>).MakeGenericType(type);
                var store = (IFactStore) _lifetimeScope.Resolve(storeType);
                return store.AppendAtomically(uowFacts);
            });
            await Task.WhenAll(storeTasks);
        }

        private IEnumerable<IFact> FactFindAndRaiseAllTheFacts()
        {
            while (true)
            {
                var facts = _aggregateRootsInTransaction
                        .SelectMany(item => item.GetAndClearFacts())
                        .ToArray();

                if (facts.None())
                    yield break;

                foreach (var fact in facts)
                {
                    _domainFactBroker.Raise((dynamic) fact);
                    yield return fact;
                }
            }
        }

    }

}