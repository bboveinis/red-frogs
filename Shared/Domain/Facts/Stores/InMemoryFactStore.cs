﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ConfigInjector.Extensions;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts.Stores
{
 
    public class InMemoryFactStore<T> : IFactStore<T> where T : AggregateRoot
    {
        private readonly ConcurrentDictionary<Guid, ConcurrentBag<UnitOfWorkFact>> _facts = new ConcurrentDictionary<Guid, ConcurrentBag<UnitOfWorkFact>>();

        public IReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>> GetFactStream()
        {
            var facts = _facts
                .ToDictionary(f => f.Key, f => OrderAndUnwrapFacts(f.Value));
            return new ReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>>(facts);
        }

        public IReadOnlyList<FactAbout<T>> GetFactStream(Guid aggregateId)
        {
            ConcurrentBag<UnitOfWorkFact> facts;
            if (_facts.TryGetValue(aggregateId, out facts))
                return OrderAndUnwrapFacts(facts);
            throw new Exception("Facts for type {0}, id {1} not found".FormatWith(typeof(T).FullName, aggregateId));
        }

        private static IReadOnlyList<FactAbout<T>> OrderAndUnwrapFacts(ConcurrentBag<UnitOfWorkFact> facts)
        {
            return facts
                .ToArray()
                .OrderBy(f => f.Timestamp)
                .ThenBy(f => f.Ordinal)
                .Select(f => f.Fact)
                .Cast<FactAbout<T>>()
                .ToArray();
        }


        public async Task AppendAtomically(IReadOnlyList<UnitOfWorkFact> facts)
        {
            await Task.Run(() =>
            {
                foreach (var fact in facts.Where(f => f.Fact.ForType == typeof (T)))
                    _facts.AddOrUpdate(fact.Fact.AggregateId,
                        id => new ConcurrentBag<UnitOfWorkFact>(new[] {fact}),
                        (id, list) =>
                        {
                            list.Add(fact);
                            return list;
                        });
            });
        }
    }
}