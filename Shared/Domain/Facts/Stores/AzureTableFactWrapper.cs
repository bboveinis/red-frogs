using System;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts.Stores
{
    public class AzureTableFactWrapper<T> : TableEntity where T : AggregateRoot
    {
// ReSharper disable once StaticFieldInGenericType
        private readonly static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        };

        private IFact _fact;

        public AzureTableFactWrapper()
        {
        }

        public AzureTableFactWrapper(UnitOfWorkFact fact)
        {
            PartitionKey = fact.Fact.AggregateId.ToString();
            RowKey = fact.UnitOfWorkId + "-" + fact.Ordinal;
            Timestamp = fact.Timestamp;
            UnitOfWorkId = fact.UnitOfWorkId;
            Ordinal = fact.Ordinal;
            _fact = fact.Fact;
            FactJson = JsonConvert.SerializeObject(fact.Fact, JsonSerializerSettings);
        }

        public int Ordinal { get; set; }
        public Guid UnitOfWorkId { get; set; }
        public string FactJson { get; set; }

        public IFact Fact
        {
            get
            {
                if (_fact == null)
                    _fact = JsonConvert.DeserializeObject<IFact>(FactJson, JsonSerializerSettings);
                return _fact;
            }
        }
    }
}