﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Domain.Facts.Stores
{
    public class AzureTableFactStore<T> : IFactStore<T> where T : AggregateRoot
    {
        private readonly Lazy<CloudTable> _table;
        private readonly string _tableName = typeof (T).Name.Replace(".", "").ToLower();

        public AzureTableFactStore(ICloudStorage cloudStorage)
        {
            _table = new Lazy<CloudTable>(() => cloudStorage.GetTable(_tableName));
        }

        public async Task AppendAtomically(IReadOnlyList<UnitOfWorkFact> facts)
        {
            var batch = new TableBatchOperation();
            foreach(var fact in facts.Where(f => f.Fact.ForType == typeof(T)))
                batch.Insert(new AzureTableFactWrapper<T>(fact));
            await _table.Value.ExecuteBatchAsync(batch);
        }

        public IReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>> GetFactStream()
        {
            var query = new TableQuery<AzureTableFactWrapper<T>>();
            var dict = ExecuteQuery(query).GroupBy(q => q.AggregateId)
                .ToDictionary(g => g.Key, g => (IReadOnlyList<FactAbout<T>>) g.ToArray());
            return new ReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>>(dict);
        }

        public IReadOnlyList<FactAbout<T>> GetFactStream(Guid aggregateId)
        {
            var query = new TableQuery<AzureTableFactWrapper<T>>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, aggregateId.ToString()));
            return ExecuteQuery(query);
        }

        private IReadOnlyList<FactAbout<T>> ExecuteQuery(TableQuery<AzureTableFactWrapper<T>> query)
        {
            return OrderAndUnwrapFacts(_table.Value.ExecuteQuery(query));
        }

        private static IReadOnlyList<FactAbout<T>> OrderAndUnwrapFacts(IEnumerable<AzureTableFactWrapper<T>> facts)
        {
            return facts
                .ToArray()
                .OrderBy(f => f.Timestamp)
                .ThenBy(f => f.Ordinal)
                .Select(f => f.Fact)
                .Cast<FactAbout<T>>()
                .ToArray();
        }
    }
}