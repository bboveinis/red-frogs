﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts.Stores
{
    public interface IFactStore
    {
        Task AppendAtomically(IReadOnlyList<UnitOfWorkFact> facts);
    }

    public interface IFactStore<T> : IFactStore where T : AggregateRoot
    {
        IReadOnlyDictionary<Guid, IReadOnlyList<FactAbout<T>>> GetFactStream();
        IReadOnlyList<FactAbout<T>> GetFactStream(Guid aggregateId);
    }

}