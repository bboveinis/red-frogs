﻿using System;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain.Facts
{
    public interface IFact
    {
        Guid AggregateId { get; }
        Type ForType { get; }
    }

    [Serializable]
    public abstract class FactAbout<T> : IFact where T : AggregateRoot
    {
        protected FactAbout(T aggregateRoot)
        {
            AggregateId = aggregateRoot.Id;
        }

        protected FactAbout()
        {
        }

        public Guid AggregateId { get; set; }

        public Type ForType
        {
            get { return typeof (T); }
        }
    }
}