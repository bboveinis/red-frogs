﻿using System.Linq;
using Autofac;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Domain.DomainEvents;
using RedFrogs.Shared.Domain.Facts.Stores;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Domain
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(LongTermAggregateStore<>))
             .As(typeof(ILongTermAggregateStore<>))
             .SingleInstance();

            builder.RegisterGeneric(typeof(LifetimeScopeAggregateStore<>))
             .As(typeof(ILifetimeScopeAggregateStore<>))
             .InstancePerLifetimeScope();

            //builder.RegisterGeneric(typeof(InMemoryFactStore<>))
            builder.RegisterGeneric(typeof(AzureTableFactStore<>))
                .As(typeof(IFactStore<>))
                .SingleInstance();

            builder.RegisterGeneric(typeof(AggregateRebuilder<>))
                .As(typeof(IAggregateRebuilder<>))
                .SingleInstance();

            builder.RegisterAssemblyTypes(InterestingAssemblies.Primary)
                .Where(t => t.IsClass && t.IsAssignableTo<IHandleDomainFact>())
                .InstancePerDependency()
                .AsImplementedInterfaces();
        }
    }
}