﻿using Autofac;
using Nimbus.InfrastructureContracts;
using RedFrogs.Shared.Domain.Aggregates;
using RedFrogs.Shared.Infrastructure;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Shared.Domain.Handlers.WhenNewFactsAreCreated
{
    public class ThenTheLongTermAggregateStoreIsInvalidated : IHandleMulticastEvent<NewFactsEvent>
    {
        private readonly ITypeResolver _typeResolver;
        private readonly ILifetimeScope _lifetimeScope;

        public ThenTheLongTermAggregateStoreIsInvalidated(ILifetimeScope lifetimeScope, ITypeResolver typeResolver)
        {
            _lifetimeScope = lifetimeScope;
            _typeResolver = typeResolver;
        }

        public void Handle(NewFactsEvent busEvent)
        {
            foreach (var group in busEvent.Facts)
            {
                var type = _typeResolver.Resolve(group.Key);
                if (type != null)
                {
                    var aggregateStoreType = typeof (ILongTermAggregateStore<>).MakeGenericType(type);
                    var aggregateStore = (ILongTermAggregateStore) _lifetimeScope.Resolve(aggregateStoreType);
                    if (aggregateStore != null)
                        aggregateStore.Invalidate(group.Value);
                }
            }
        }
    }
}