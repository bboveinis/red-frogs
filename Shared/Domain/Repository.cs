﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RedFrogs.Shared.Domain.Aggregates;

namespace RedFrogs.Shared.Domain
{
    public interface IRepository<T> where T : AggregateRoot
    {
        T Get(Guid id);
        void Add(T item);
        IReadOnlyList<T> All();
        Task CommitUnitOfWork();
    }

    public class Repository<T> : IRepository<T> where T : AggregateRoot
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILifetimeScopeAggregateStore<T> _lifetimeScopeAggregateStore;

        public Repository(IUnitOfWork unitOfWork, ILifetimeScopeAggregateStore<T> lifetimeScopeAggregateStore)
        {
            _unitOfWork = unitOfWork;
            _lifetimeScopeAggregateStore = lifetimeScopeAggregateStore;
        }

        public T Get(Guid id)
        {
            var item = _lifetimeScopeAggregateStore.Get(id);
            if (item != null) 
                _unitOfWork.EnlistInTransaction(item);

            return item;
        }

        public void Add(T item)
        {
            _unitOfWork.EnlistInTransaction(item);
        }

        public IReadOnlyList<T> All()
        {
            var items = _lifetimeScopeAggregateStore.All();
            _unitOfWork.EnlistInTransaction(items);
            return items;
        }

        public Task CommitUnitOfWork()
        {
            return _unitOfWork.Commit();
        }
    }
}