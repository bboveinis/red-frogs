﻿using System;
using Nimbus.MessageContracts;

namespace RedFrogs.Shared.Messages
{
    public class JobConfirmedEvent : IBusEvent
    {
        public Guid JobId { get; set; }
    }
}
