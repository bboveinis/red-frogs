﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Nimbus.MessageContracts;
using RedFrogs.Shared.Domain;

namespace RedFrogs.Shared.Messages
{
    [DataContract]
    public class NewFactsEvent : IBusEvent
    {
        [DataMember]
        public Dictionary<string, Guid[]> Facts { get; set; }

        public NewFactsEvent(IReadOnlyList<UnitOfWorkFact> facts)
        {
            Facts = facts.GroupBy(f => f.Fact.ForType).ToDictionary(g => g.Key.FullName, g => g.Select(f => f.Fact.AggregateId).ToArray());
        }
    }
}