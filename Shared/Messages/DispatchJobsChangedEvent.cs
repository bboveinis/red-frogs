﻿using Nimbus.MessageContracts;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Messages
{
    public class DispatchJobsChangedEvent : ICanMerge<DispatchJobsChangedEvent>, IBusEvent
    {
        public void MergeIntoMe(DispatchJobsChangedEvent other)
        {
        }
    }
}