﻿using Autofac;
using Serilog;

namespace RedFrogs.Shared.Infrastructure
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(Log.Logger).AsImplementedInterfaces();
        }
    }
}