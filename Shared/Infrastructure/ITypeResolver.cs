﻿using System;

namespace RedFrogs.Shared.Infrastructure
{
    public interface ITypeResolver
    {
        Type Resolve(string name);
    }
}