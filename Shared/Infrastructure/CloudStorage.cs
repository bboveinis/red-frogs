﻿using System;
using Autofac;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using RedFrogs.Shared.Config;

namespace RedFrogs.Shared.Infrastructure
{
    public interface ICloudStorage
    {
        CloudTable GetTable(string name);
    }

    [SingleInstance]
    public class CloudStorage : ICloudStorage
    {
        private readonly AzureStorageConnectionString _azureStorageConnectionString;
        private readonly Lazy<CloudStorageAccount> _account;
        private readonly Lazy<CloudTableClient> _tableClient;

        public CloudStorage(AzureStorageConnectionString azureStorageConnectionString)
        {
            _azureStorageConnectionString = azureStorageConnectionString;
            _account = new Lazy<CloudStorageAccount>(CreateAccount);
            _tableClient = new Lazy<CloudTableClient>(CreateTableClient);
        }

        private CloudTableClient CreateTableClient()
        {
            return _account.Value.CreateCloudTableClient();
        }

        private CloudStorageAccount CreateAccount()
        {
            return CloudStorageAccount.Parse(_azureStorageConnectionString);
        }

        public CloudTable GetTable(string name)
        {
            var table = _tableClient.Value.GetTableReference(name);
            table.CreateIfNotExists();
            return table;
        }
    }
}