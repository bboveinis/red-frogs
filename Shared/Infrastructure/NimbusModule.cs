﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Autofac;
using Microsoft.WindowsAzure.ServiceRuntime;
using Nimbus;
using Nimbus.Configuration;
using Nimbus.Infrastructure;
using Nimbus.InfrastructureContracts;
using Nimbus.Logger.Serilog;
using RedFrogs.Shared.Config;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Shared.Infrastructure
{
    public abstract class NimbusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var parentAssembly = GetType().Assembly;

            var name = RoleEnvironment.IsAvailable
                ? RoleEnvironment.CurrentRoleInstance.Role.Name
                : parentAssembly.GetName().Name;

            var id = RoleEnvironment.IsAvailable
                ? Regex.Replace(RoleEnvironment.CurrentRoleInstance.Id, "[^A-Za-z0-9]", "_")
                : Environment.MachineName + Process.GetCurrentProcess().Id;

            var handlerTypesProvider = new AssemblyScanningTypeProvider(parentAssembly, typeof(JobConfirmedEvent).Assembly);

            builder.RegisterInstance(new SerilogStaticLogger()).AsImplementedInterfaces();
            builder.RegisterNimbus(handlerTypesProvider);
            builder.Register(c => new BusBuilder()
                                 .Configure()
                                 .WithConnectionString(c.Resolve<AzureBusConnectionString>())
                                 .WithNames(name, id)
                                 .WithTypesFrom(handlerTypesProvider)
                                 .WithAutofacDefaults(c)
                                 .Build()
                            )
                   .AutoActivate()
                   .As<Bus>()
                   .OnActivated(c => c.Instance.Start())
                   .SingleInstance();
           
            builder.Register(c => new SerilogBusWrapper(c.Resolve<Bus>(), c.Resolve<ILogger>()))
                    .As<IBus>()
                    .SingleInstance();
        }
    }
}