﻿namespace RedFrogs.Shared.Infrastructure
{
    public interface ICanMerge<in T>
    {
        void MergeIntoMe(T other);
    }
}