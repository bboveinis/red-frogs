define('Home.IndexViewModel', [], function() {
    return function() {
        var self = this;

        self.hub = $.connection.dispatchDashboardHub;
        self.hub.client.pleaseRefresh = function() {
            self.getDispatchJobs();
        };

        self.dispatchJobs = ko.observableArray([]);

        self.getDispatchJobs = function() {
            $.get('/api/DispatchJobs')
                .success(self.dispatchJobs);
        };

        self.dispatchAJob = function(item) {
            $.post('/api/DispatchJob/' + item.id)
                .success(function() {
                toastr.success("Job dispatched");
            });
        };

        self.getDispatchJobs();
        $.connection.hub.start();
    };
});