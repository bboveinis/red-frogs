﻿using System;
using RedFrogs.Shared.Domain.Facts;

namespace RedFrogs.Dispatch.Domain.Dispatches.Facts
{
    [Serializable]
    public class DispatchJobCreatedFact : FactAbout<DispatchJob>
    {
        public Guid JobId { get; set; }

        public DispatchJobCreatedFact()
        {
            
        }

        public DispatchJobCreatedFact(DispatchJob aggregateRoot, Guid jobId) : base(aggregateRoot)
        {
            JobId = jobId;
        }
    }
}