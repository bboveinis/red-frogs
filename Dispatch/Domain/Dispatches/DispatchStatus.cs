﻿namespace RedFrogs.Dispatch.Domain.Dispatches
{
    public enum DispatchStatus
    {
        Pending,
        Dispatched,
        Completed
    }
}