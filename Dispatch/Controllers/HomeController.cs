﻿using System.Web.Mvc;

namespace RedFrogs.Dispatch.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
