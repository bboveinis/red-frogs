﻿using System.Linq;
using System.Web.Http;
using RedFrogs.Dispatch.Domain.Dispatches;
using RedFrogs.Shared.Domain;

namespace RedFrogs.Dispatch.Controllers.Api
{
    public class DispatchJobsController : ApiController
    {
        private readonly IRepository<DispatchJob> _dispatchJobRepository;

        public DispatchJobsController(IRepository<DispatchJob>  dispatchJobRepository)
        {
            _dispatchJobRepository = dispatchJobRepository;
        }

        public object Get()
        {
            return from d in _dispatchJobRepository.All()
                select new
                {
                    d.Id,
                    d.JobId,
                    d.Status
                };
        }
    }
}