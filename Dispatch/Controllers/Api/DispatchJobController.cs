using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using RedFrogs.Dispatch.Domain.Dispatches;
using RedFrogs.Shared.Domain;

namespace RedFrogs.Dispatch.Controllers.Api
{
    public class DispatchJobController : ApiController
    {
        private readonly IRepository<DispatchJob> _dispatchJobRepository;

        public DispatchJobController(IRepository<DispatchJob> dispatchJobRepository)
        {
            _dispatchJobRepository = dispatchJobRepository;
        }

        public async Task Post(Guid id)
        {
            var job = _dispatchJobRepository.Get(id);
            if(job == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { ReasonPhrase = "Dispatch job not found"});

            job.Dispatch();
            await _dispatchJobRepository.CommitUnitOfWork();
        }
    }
}