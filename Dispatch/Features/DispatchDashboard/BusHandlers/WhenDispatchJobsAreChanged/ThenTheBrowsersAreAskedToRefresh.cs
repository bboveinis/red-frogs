﻿using Microsoft.AspNet.SignalR;
using Nimbus.InfrastructureContracts;
using Nimbus.MessageContracts;
using RedFrogs.Shared.Domain;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Dispatch.Features.DispatchDashboard.BusHandlers.WhenDispatchJobsAreChanged
{
    public class ThenTheBrowsersAreAskedToRefresh : IHandleMulticastEvent<DispatchJobsChangedEvent>
    {
        public void Handle(DispatchJobsChangedEvent busEvent)
        {
            GlobalHost.ConnectionManager.GetHubContext<DispatchDashboardHub>()
                .Clients.All.pleaseRefresh();
        }
    }
}