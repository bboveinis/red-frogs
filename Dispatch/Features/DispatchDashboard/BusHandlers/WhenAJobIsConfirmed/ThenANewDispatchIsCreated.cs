﻿using Nimbus.InfrastructureContracts;
using RedFrogs.Dispatch.Domain.Dispatches;
using RedFrogs.Shared.Domain;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Dispatch.Handlers.WhenAJobIsConfirmed
{
    public class ThenANewDispatchIsCreated : IHandleCompetingEvent<JobConfirmedEvent>
    {
        private readonly IRepository<DispatchJob> _dispatchJobRepository;

        public ThenANewDispatchIsCreated(IRepository<DispatchJob> dispatchJobRepository)
        {
            _dispatchJobRepository = dispatchJobRepository;
        }

        public void Handle(JobConfirmedEvent busEvent)
        {
            _dispatchJobRepository.Add(DispatchJob.Create(busEvent.JobId));
            _dispatchJobRepository.CommitUnitOfWork().Wait();
        }
    }
}