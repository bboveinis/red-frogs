﻿using RedFrogs.Dispatch.Domain.Dispatches.Facts;
using RedFrogs.Shared.Domain.DomainEvents;
using RedFrogs.Shared.Infrastructure;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Dispatch.Features.DispatchDashboard.FactHandlers.WhenADispatchJobIsCreated
{
    public class ThenADispatchJobsChangedEventIsRaised : IHandleDomainFact<DispatchJobCreatedFact>
    {
        private readonly IUnitOfWorkBus _unitOfWorkBus;

        public ThenADispatchJobsChangedEventIsRaised(IUnitOfWorkBus unitOfWorkBus)
        {
            _unitOfWorkBus = unitOfWorkBus;
        }

        public void Handle(DispatchJobCreatedFact fact)
        {
            _unitOfWorkBus.PublishOnCommit(new DispatchJobsChangedEvent());
        }
    }
}