﻿using RedFrogs.Dispatch.Domain.Dispatches.Facts;
using RedFrogs.Shared.Domain;
using RedFrogs.Shared.Domain.DomainEvents;
using RedFrogs.Shared.Infrastructure;
using RedFrogs.Shared.Messages;

namespace RedFrogs.Dispatch.Features.DispatchDashboard.FactHandlers.WhenADispatchJobIsDispatched
{
    public class ThenADispatchJobsChangedEventIsRaised : IHandleDomainFact<DispatchJobDispatchedFact>
    {
        private readonly IUnitOfWorkBus _unitOfWorkBus;
        private readonly IUnitOfWork _unitOfWork;

        public ThenADispatchJobsChangedEventIsRaised(IUnitOfWorkBus unitOfWorkBus, IUnitOfWork unitOfWork)
        {
            _unitOfWorkBus = unitOfWorkBus;
            _unitOfWork = unitOfWork;
        }

        public void Handle(DispatchJobDispatchedFact fact)
        {
            _unitOfWorkBus.PublishOnCommit(new DispatchJobsChangedEvent());
        }
    }
}