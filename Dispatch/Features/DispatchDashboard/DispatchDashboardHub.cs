﻿using Microsoft.AspNet.SignalR;

namespace RedFrogs.Dispatch.Features.DispatchDashboard
{
    public class DispatchDashboardHub : Hub
    {
        public void Send()
        {
            Clients.All.broadcastMessage("Foo");
        }
    }
}