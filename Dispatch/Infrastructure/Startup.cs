﻿using Microsoft.Owin;
using Owin;
using RedFrogs.Dispatch.Infrastructure;

[assembly: OwinStartup(typeof(Startup))]
namespace RedFrogs.Dispatch.Infrastructure
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }

}