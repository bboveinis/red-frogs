﻿using System;
using Autofac;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Dispatch.Infrastructure
{
    [SingleInstance]
    public class TypeResolver : ITypeResolver
    {
        public Type Resolve(string name)
        {
            return Type.GetType(name);
        }
    }
}