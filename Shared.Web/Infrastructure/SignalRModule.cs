using Autofac;
using Autofac.Integration.SignalR;
using Microsoft.AspNet.SignalR;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Web.Infrastructure
{
    public class SignalRModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterHubs(InterestingAssemblies.Primary);
            builder.RegisterType<Startup>().As<IStartable>();
        }

        public class Startup : IStartable
        {
            private readonly ILifetimeScope _lifetimeScope;

            public Startup(ILifetimeScope lifetimeScope)
            {
                _lifetimeScope = lifetimeScope;
            }

            public void Start()
            {
                GlobalHost.DependencyResolver = new AutofacDependencyResolver(_lifetimeScope);
            }
        }
    }
}