﻿using System.Web;
using Autofac;
using RedFrogs.Shared.Config;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Web.Infrastructure
{
    public class AutofacConfig
    {
        public static void BuildContainer<T>() where T : HttpApplication
        {
            var webAssembly = typeof (T).Assembly;
            InterestingAssemblies.AddPrimary(webAssembly);
            InterestingAssemblies.AddSupport(typeof(ConfigModule).Assembly);
            InterestingAssemblies.AddSupport(typeof(AutofacConfig).Assembly);

            var builder = new ContainerBuilder();
            builder.RegisterByAttributes(InterestingAssemblies.All);
            builder.RegisterAssemblyModules(InterestingAssemblies.All);
            
            builder.Build();

        }
    }
}