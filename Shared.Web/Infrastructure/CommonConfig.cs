﻿using System.Web;

namespace RedFrogs.Shared.Web.Infrastructure
{
    public class CommonConfig
    {
        public static void Configure<T>() where T : HttpApplication
        {
            LogConfig.Configure();
            AutofacConfig.BuildContainer<T>();
        }
    }
}