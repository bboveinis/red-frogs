using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using RedFrogs.Shared.Infrastructure;

namespace RedFrogs.Shared.Web.Infrastructure
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(InterestingAssemblies.Primary);
            builder.RegisterApiControllers(InterestingAssemblies.Primary);
            builder.RegisterType<Startup>().As<IStartable>();
        }

        public class Startup : IStartable
        {
            private readonly ILifetimeScope _lifetimeScope;

            public Startup(ILifetimeScope lifetimeScope)
            {
                _lifetimeScope = lifetimeScope;
            }

            public void Start()
            {
                DependencyResolver.SetResolver(new AutofacDependencyResolver(_lifetimeScope));
                GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(_lifetimeScope);
            }
        }
    }
}