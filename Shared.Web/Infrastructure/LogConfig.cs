﻿using Seq;
using Serilog;

namespace RedFrogs.Shared.Web.Infrastructure
{
    internal class LogConfig
    {
        internal static void Configure()
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Verbose()
               .WriteTo.Glimpse()
               .WriteTo.Seq("http://localhost:5341")
               .CreateLogger();
        }
    }
}